#!/bin/bash

NAMESPACE=$1
CHART_NAME=$2

helm uninstall -n $NAMESPACE $CHART_NAME

# velero.io 그룹 crd 삭제cmd
kubectl delete crd backuprepositories.velero.io
kubectl delete crd backups.velero.io
kubectl delete crd backupstoragelocations.velero.io
kubectl delete crd datadownloads.velero.io
kubectl delete crd datauploads.velero.io
kubectl delete crd deletebackuprequests.velero.io
kubectl delete crd downloadrequests.velero.io
kubectl delete crd podvolumebackups.velero.io
kubectl delete crd podvolumerestores.velero.io
kubectl delete crd restores.velero.io
kubectl delete crd schedules.velero.io
kubectl delete crd serverstatusrequests.velero.io
kubectl delete crd volumesnapshotlocations.velero.io

# edge-backup.kubevirt.io 삭제cmd
kubectl delete crd backuplocations.edge-backup.kubevirt.io
kubectl delete crd backups.edge-backup.kubevirt.io
kubectl delete crd restores.edge-backup.kubevirt.io
